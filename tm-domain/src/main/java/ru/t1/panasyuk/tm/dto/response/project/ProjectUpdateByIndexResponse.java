package ru.t1.panasyuk.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIndexResponse extends AbstractProjectResponse {

    @Nullable
    private ProjectDTO project;

    public ProjectUpdateByIndexResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }

    public ProjectUpdateByIndexResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}