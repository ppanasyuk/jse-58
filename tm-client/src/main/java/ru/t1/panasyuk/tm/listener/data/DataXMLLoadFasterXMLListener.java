package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataXMLLoadFasterXMLRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataXMLLoadFasterXMLListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    private static final String NAME = "data-load-xml-fasterxml";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXMLLoadFasterXMLListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXMLLoadFasterXMLRequest request = new DataXMLLoadFasterXMLRequest(getToken());
        domainEndpoint.loadDataXMLFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
