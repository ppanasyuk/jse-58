package ru.t1.panasyuk.tm.service;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.*;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;

import java.util.List;

@DisplayName("Тестирование сервиса DomainService")
public class DomainServiceTest extends AbstractSchemeTest {

    @NotNull
    private UserDTO test;

    @NotNull
    private UserDTO admin;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initService() throws Exception {

        test = getUserDtoService().create("TEST", "TEST", "TEST@TEST.ru");
        admin = getUserDtoService().create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        @NotNull final ProjectDTO project1 = getProjectDtoService().create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final ProjectDTO project2 = getProjectDtoService().create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final ProjectDTO project3 = getProjectDtoService().create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final TaskDTO task1 = getTaskDtoService().create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        getTaskDtoService().update(task1);
        @NotNull final TaskDTO task2 = getTaskDtoService().create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        getTaskDtoService().update(task2);
        @NotNull final TaskDTO task3 = getTaskDtoService().create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        getTaskDtoService().update(task3);
        @NotNull final TaskDTO task4 = getTaskDtoService().create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        getTaskDtoService().update(task4);
    }

    private IProjectDtoService getProjectDtoService() {
        return context.getBean(IProjectDtoService.class);
    }

    private ITaskDtoService getTaskDtoService() {
        return context.getBean(ITaskDtoService.class);
    }

    private IUserDtoService getUserDtoService() {
        return context.getBean(IUserDtoService.class);
    }

    private IDomainService getDomainService() {
        return context.getBean(IDomainService.class);
    }

    @After
    public void afterTest() throws Exception {
        getTaskDtoService().clear(test.getId());
        getTaskDtoService().clear(admin.getId());
        getProjectDtoService().clear(test.getId());
        getProjectDtoService().clear(admin.getId());
        getUserDtoService().remove(admin);
        getUserDtoService().remove(test);
    }

    @Test
    @DisplayName("Сохранение и загрузка бэкапа")
    public void dataBackupTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataBackup();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataBackup();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Base64")
    public void dataBase64Test() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataBase64();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataBase64();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Binary")
    public void dataBinaryTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataBinary();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataBinary();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя FasterXML")
    public void dataJsonFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataJsonFasterXML();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataJsonFasterXML();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Json используя JaxB")
    public void dataJsonJaxBTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataJsonJaxB();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя FasterXML")
    public void dataXMLFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataXMLFasterXML();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataXMLFasterXML();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате XML используя JaxB")
    public void dataXMLJaxBTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataXMLJaxB();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataXMLJaxB();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

    @Test
    @DisplayName("Сохранение и загрузка данных в фомате Yaml используя FasterXML")
    public void dataYamlFasterXMLTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = getProjectDtoService().findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @Nullable final List<TaskDTO> tasks = getTaskDtoService().findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        @Nullable final List<UserDTO> users = getUserDtoService().findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
        getDomainService().saveDataYamlFasterXML();
        getTaskDtoService().clear();
        getProjectDtoService().clear();
        getUserDtoService().clear();
        Assert.assertEquals(0, getProjectDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getTaskDtoService().getSize(test.getId()));
        Assert.assertEquals(0, getUserDtoService().getSize());
        getDomainService().loadDataYamlFasterXML();
        Assert.assertEquals(projects, getProjectDtoService().findAll(test.getId()));
        Assert.assertEquals(tasks, getTaskDtoService().findAll(test.getId()));
        Assert.assertEquals(users, getUserDtoService().findAll());
    }

}