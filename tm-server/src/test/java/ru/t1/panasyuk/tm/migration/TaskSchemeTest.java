package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@DisplayName("Тестирование создания схемы для задач")
public class TaskSchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("task");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final TaskDTO task = createTask();
        Assert.assertNotNull(task);
        int countOfTasks = getCountOfTasks();
        Assert.assertEquals(1, countOfTasks);
        deleteTask(task);
    }

    @NotNull
    private TaskDTO createTask() {
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskRepository.add(taskDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskDTO;
    }

    private int getCountOfTasks() {
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        entityManager.getTransaction().begin();
        final int count = taskRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteTask(@NotNull final TaskDTO task) {
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        entityManager.getTransaction().begin();
        taskRepository.remove(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}