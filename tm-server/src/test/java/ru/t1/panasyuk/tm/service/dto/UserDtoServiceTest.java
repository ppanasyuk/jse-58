package ru.t1.panasyuk.tm.service.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.IAuthDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IUserDtoService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

@DisplayName("Тестирование сервиса UserDtoService")
public class UserDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IAuthDtoService authService;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private UserDTO admin;

    @NotNull
    private UserDTO test;

    @NotNull
    private UserDTO user;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskDtoService projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        @NotNull final ISessionDtoService sessionService = context.getBean(ISessionDtoService.class);
        authService = context.getBean(IAuthDtoService.class);
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("TEST_ADMIN_1", "TEST_ADMIN_1", "ADMIN@TEST.ru", Role.ADMIN);
        admin.setEmail("ADMIN@TEST_ADMIN_1");
        userService.update(admin);
        user = userService.create("USER", "USER", "ADMIN@TEST.ru", Role.ADMIN);
        user.setEmail("USER@TEST_USER");
        userService.update(user);
        userList = new ArrayList<>();
        userList.add(test);
        userList.add(admin);
        userList.add(user);
    }

    @After
    public void afterTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            userService.remove(user);
        }
    }

    @Test
    @DisplayName("Добавление пользователя")
    public void AddTest() throws Exception {
        int expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("TEST_LOGIN");
        user.setFirstName("TEST_FIRSTNAME");
        user.setLastName("TEST_LASTNAME");
        user.setEmail("TEST@TEST");
        user.setRole(Role.USUAL);
        user.setPasswordHash("test");
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        userService.remove(user);
    }

    @Test
    @DisplayName("Добавление пользователя")
    public void AddCollectionTest() throws Exception {
        int expectedNumberOfEntries = userService.getSize() + 2;
        @NotNull final List<UserDTO> users = new ArrayList<>();
        @NotNull final UserDTO user_1 = new UserDTO();
        user_1.setLogin("TEST_LOGIN");
        user_1.setFirstName("TEST_FIRSTNAME");
        user_1.setLastName("TEST_LASTNAME");
        user_1.setEmail("TEST@TEST");
        user_1.setRole(Role.USUAL);
        user_1.setPasswordHash("test");
        users.add(user_1);
        @NotNull final UserDTO user_2 = new UserDTO();
        user_1.setLogin("TEST_LOGIN_2");
        user_1.setFirstName("TEST_FIRSTNAME_2");
        user_1.setLastName("TEST_LASTNAME_2");
        user_1.setEmail("TEST@TEST_2");
        user_1.setRole(Role.USUAL);
        user_1.setPasswordHash("test_2");
        users.add(user_2);
        userService.add(users);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    @DisplayName("Создание пользователя")
    public void createTest() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final UserDTO user = userService.create(login, password);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        userList.add(newUser);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с пустым логином")
    public void createLoginEmptyTestNegative() throws Exception {
        userService.create("", "PASS");
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с существующим логином")
    public void createLoginExistsTestNegative() throws Exception {
        userService.create("TEST", "PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с пустым паролем")
    public void createPasswordEmptyTestNegative() throws Exception {
        userService.create("LOGIN", "");
    }

    @Test
    @DisplayName("Создание пользователя с Email")
    public void createWithEmailTest() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final UserDTO user = userService.create(login, password, email);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
        userList.add(newUser);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с Email и пустым логином")
    public void createWithEmailLoginEmptyTestNegative() throws Exception {
        userService.create("", "PASS", "EMAIL");
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с Email и существующим логином")
    public void createWithEmailLoginExistsTestNegative() throws Exception {
        userService.create("TEST", "PASS", "EMAIL");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с Email и пустым паролем")
    public void createWithEmailPasswordEmptyTestNegative() throws Exception {
        userService.create("LOGIN", "", "EMAIL");
    }

    @Test(expected = EmailExistsException.class)
    @DisplayName("Создание пользователя с уже существующим Email")
    public void createWithEmailEmailExistsTestNegative() throws Exception {
        userService.create("LOGIN", "PASS", "TEST@TEST.ru");
    }

    @Test
    @DisplayName("Создание пользователя с ролью")
    public void createWithRole() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL@TEST.ru";
        @NotNull final Role role = Role.USUAL;
        @NotNull final UserDTO user = userService.create(login, password, email, role);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(role, newUser.getRole());
        userList.add(newUser);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Создание пользователя с ролью и пустым логином")
    public void createWithRoleLoginEmptyTestNegative() throws Exception {
        userService.create("", "PASS", "ADMIN@TEST.ru", Role.USUAL);
    }

    @Test(expected = LoginExistsException.class)
    @DisplayName("Создание пользователя с ролью и существующим логином")
    public void createWithRoleLoginExistsTestNegative() throws Exception {
        userService.create("TEST", "PASS", "TEST@TEST.ru", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Создание пользователя с ролью и пустым паролем")
    public void createWithRolePasswordEmptyTestNegative() throws Exception {
        userService.create("LOGIN", "", "LOGIN@TEST.ru", Role.USUAL);
    }

    @Test
    @DisplayName("Проверка существования пользователя по Id")
    public void existByIdTrueTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            final boolean isExist = userService.existsById(user.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования пользователя по Id")
    public void existByIdFalseTest() throws Exception {
        final boolean isExist = userService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Поиск всех пользователей")
    public void findAllTest() throws Exception {
        @NotNull final List<UserDTO> users = userService.findAll();
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    @DisplayName("Поиск пользователя по логину")
    public void findByLoginTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final UserDTO foundUser = userService.findByLogin(user.getLogin());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Поиск пользователя по логину с пустым логином")
    public void findByLoginEmptyTestNegative() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Поиск пользователя по логину с Null логином")
    public void findByLoginNullTestNegative() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin(null);
    }

    @Test
    @DisplayName("Поиск пользователя по Email")
    public void findByEmailTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final UserDTO foundUser = userService.findByEmail(user.getEmail());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Id")
    public void findOneByIdTest() throws Exception {
        @Nullable UserDTO foundUser;
        for (@NotNull final UserDTO user : userList) {
            foundUser = userService.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Id равному Null")
    public void findOneByIdNullTest() throws Exception {
        @Nullable final UserDTO foundUser = userService.findOneById(null);
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по пустому Id")
    public void findOneByIdEmptyTest() throws Exception {
        @Nullable final UserDTO foundUser = userService.findOneById("");
        Assert.assertNull(foundUser);
    }

    @Test
    @DisplayName("Поиск пользователя по индексу")
    public void findOneByIndexTest() throws Exception {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final UserDTO user = userService.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по индексу превышающему границы")
    public void findOneByIndexIndexIncorrectNegative() throws Exception {
        int index = userService.getSize() + 1;
        @Nullable final UserDTO user = userService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по индексу равному Null")
    public void findOneByIndexNullIndexIncorrectNegative() throws Exception {
        @Nullable final UserDTO user = userService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Поиск пользователя по отрицательному индексу")
    public void findOneByIndexMinusIndexIncorrectNegative() throws Exception {
        @Nullable final UserDTO user = userService.findOneByIndex(-1);
    }

    @Test
    @DisplayName("Получение количества пользователей")
    public void getSizeTest() throws Exception {
        int actualSize = userService.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Проверка существования логина в системе")
    public void isLoginExistTrueTest() throws Exception {
        boolean isExist = userService.isLoginExist("TEST");
        Assert.assertTrue(isExist);
    }

    @Test
    @DisplayName("Проверка несуществования логина в системе")
    public void isLoginExistFalseTest() throws Exception {
        boolean isExist = userService.isLoginExist("ABYRVALG");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования логина Null в системе")
    public void isLoginExistNullFalseTest() throws Exception {
        boolean isExist = userService.isLoginExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования пустого логина в системе")
    public void isLoginExistEmptyFalseTest() throws Exception {
        boolean isExist = userService.isLoginExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования Email в системе")
    public void isEmailExistTrueTest() throws Exception {
        boolean isExist = userService.isEmailExist("TEST@TEST.ru");
        Assert.assertTrue(isExist);
    }

    @Test
    @DisplayName("Проверка несуществования Email в системе")
    public void isEmailExistFalseTest() throws Exception {
        boolean isExist = userService.isEmailExist("QWERTY@QWERTY");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования Email равного Null в системе")
    public void isEmailExistNullFalseTest() throws Exception {
        boolean isExist = userService.isEmailExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Проверка существования пустого Email в системе")
    public void isEmailExistEmptyFalseTest() throws Exception {
        boolean isExist = userService.isEmailExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Заблокировать пользователя по логину")
    public void lockUserByLoginTest() throws Exception {
        userService.lockUserByLogin("TEST");
        @Nullable final UserDTO user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        Assert.assertEquals(true, user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Заблокировать пользователя по логину равному Null")
    public void lockUserByLoginLoginNullTestNegative() throws Exception {
        userService.lockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Заблокировать пользователя по пустому логину")
    public void lockUserByLoginLoginEmptyTestNegative() throws Exception {
        userService.lockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Заблокировать пользователя по несуществующему логину")
    public void lockUserByLoginUserNotFoundTestNegative() throws Exception {
        userService.lockUserByLogin("123");
    }

    @Test
    @DisplayName("Удаление пользователя по логину")
    public void removeByLoginTest() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable UserDTO deletedUser = userService.removeByLogin("TEST");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByLogin("TEST");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Удаление пользователя по логину равному Null")
    public void removeByLoginLoginNullTestNegative() throws Exception {
        userService.removeByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Удаление пользователя по пустому логину")
    public void removeByLoginLoginEmptyTestNegative() throws Exception {
        userService.removeByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Удаление пользователя по логину несуществующего в системе")
    public void removeByLoginUserNotFoundTestNegative() throws Exception {
        userService.removeByLogin("123");
    }

    @Test
    @DisplayName("Удаление пользователя по Email")
    public void removeByEmailTest() throws Exception {
        @Nullable final UserDTO user = userService.findByEmail("TEST@TEST.ru");
        Assert.assertNotNull(user);
        @Nullable UserDTO deletedUser = userService.removeByEmail("TEST@TEST.ru");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByEmail("TEST@TEST.ru");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = EmailEmptyException.class)
    @DisplayName("Удаление пользователя по Email равному Null")
    public void removeByEmailEmailNullTestNegative() throws Exception {
        userService.removeByEmail(null);
    }

    @Test(expected = EmailEmptyException.class)
    @DisplayName("Удаление пользователя по пустому Email")
    public void removeByEmailEmailEmptyTestNegative() throws Exception {
        userService.removeByEmail("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Удаление пользователя по Email несуществующему в системе")
    public void removeByEmailUserNotFoundTestNegative() throws Exception {
        userService.removeByEmail("123");
    }

    @Test
    @DisplayName("Установить пользователю пароль")
    public void setPasswordTest() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable final UserDTO updatedUser = userService.setPassword(user.getId(), "NEW_PASS");
        Assert.assertNotNull(updatedUser);
        @Nullable final String token = authService.login("TEST", "NEW_PASS");
        Assert.assertNotNull(token);
        authService.logout(token);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Установить пользователю пароль по Null id")
    public void setPasswordIdNullTestNegative() throws Exception {
        userService.setPassword(null, "NEW_PASS");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Установить пользователю пароль по пустому Id")
    public void setPasswordIdEmptyTestNegative() throws Exception {
        userService.setPassword("", "NEW_PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Установить пользователю Null пароль")
    public void setPasswordPasswordNullTestNegative() throws Exception {
        userService.setPassword("ID", null);
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Установить пользователю пустой пароль")
    public void setPasswordPasswordEmptyTestNegative() throws Exception {
        userService.setPassword("ID", "");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Установить несуществующему пользователю пароль")
    public void setPasswordUserNotFoundTestNegative() throws Exception {
        userService.setPassword("USER123", "NEW_PASS");
    }

    @Test
    @DisplayName("Удалить пользователя")
    public void removeTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO deletedUser = userService.remove(user);
            Assert.assertNotNull(deletedUser);
            @Nullable final UserDTO deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить Null пользователя")
    public void removeEntityNullNotFoundTestNegative() throws Exception {
        @Nullable final UserDTO deletedUser = userService.remove(null);
    }

    @Test
    @DisplayName("Удалить пользователя по Id")
    public void removeByIdTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO deletedUser = userService.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final UserDTO deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить пользователя по Id равному Null")
    public void removeByIdIdNullTestNegative() throws Exception {
        userService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить пользователя по пустому Id")
    public void removeByIdIdEmptyTestNegative() throws Exception {
        userService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующего пользователя")
    public void removeByIdEntityNotFoundTestNegative() throws Exception {
        userService.removeById("123321");
    }

    @Test
    @DisplayName("Удалить пользователя по индексу")
    public void removeByIndexTest() throws Exception {
        @Nullable final UserDTO user = userService.findOneByIndex(1);
        Assert.assertNotNull(user);
        @NotNull final String id = user.getId();
        @Nullable final UserDTO deletedUser = userService.removeByIndex(1);
        Assert.assertNotNull(deletedUser);
        @Nullable final UserDTO deletedUserInDB = userService.findOneById(id);
        Assert.assertNull(deletedUserInDB);
        final int countOfUsers = userService.getSize();
        Assert.assertTrue(countOfUsers > 0);
        @Nullable final UserDTO lastUser = userService.findOneByIndex(countOfUsers);
        Assert.assertNotNull(lastUser);
        @NotNull final String lastUserId = user.getId();
        @Nullable final UserDTO deletedLastUser = userService.removeByIndex(countOfUsers);
        Assert.assertNotNull(deletedLastUser);
        @Nullable final UserDTO deletedLastUserInDB = userService.findOneById(lastUserId);
        Assert.assertNull(deletedLastUserInDB);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить пользователя по Null индексу")
    public void removeByIndexNullIndexIncorrectTestNegative() throws Exception {
        userService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить пользователя по отрицательному индексу")
    public void removeByIndexMinusIndexIncorrectTestNegative() throws Exception {
        userService.removeByIndex(-1);
    }

    @Test
    @DisplayName("Разблокировать пользователя по логину")
    public void unlockUserByLoginTest() throws Exception {
        userService.lockUserByLogin("TEST");
        @Nullable final UserDTO lockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(lockedUser);
        Assert.assertEquals(true, lockedUser.getLocked());
        userService.unlockUserByLogin("TEST");
        @Nullable final UserDTO unlockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(unlockedUser);
        Assert.assertEquals(false, unlockedUser.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Разблокировать пользователя по Null логину")
    public void unlockUserByLoginLoginNullTestNegative() throws Exception {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Разблокировать пользователя по пустому логину")
    public void unlockUserByLoginLoginEmptyTestNegative() throws Exception {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Разблокировать несуществующего пользователя по логину")
    public void unlockUserByLoginUserNotFoundTestNegative() throws Exception {
        userService.unlockUserByLogin("123");
    }

    @Test
    @DisplayName("Обновление пользователя")
    public void updateUser() throws Exception {
        @NotNull final String firstName = "NEW_FIRST_NAME";
        @NotNull final String lastName = "NEW_LAST_NAME";
        @NotNull final String middleName = "NEW_MIDDLE_NAME";
        @Nullable final UserDTO user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        userService.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO updatedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление пользователя по Null id")
    public void updateUserLoginNullTestNegative() throws Exception {
        userService.updateUser(null, "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление пользователя по пустому id")
    public void updateUserLoginEmptyTestNegative() throws Exception {
        userService.updateUser("", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = UserNotFoundException.class)
    @DisplayName("Обновление несуществующего пользователя")
    public void updateUserUserNotFoundTestNegative() throws Exception {
        userService.updateUser("123321", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

}