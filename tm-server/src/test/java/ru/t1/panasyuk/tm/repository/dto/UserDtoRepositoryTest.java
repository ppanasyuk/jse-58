package ru.t1.panasyuk.tm.repository.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@DisplayName("Тестирование репозитория для пользователей DTO")
public class UserDtoRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private IUserDtoRepository userRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws LiquibaseException, SQLException, IOException {
        liquibase.update("scheme");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = context.getBean(IUserDtoRepository.class);
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("USER" + i);
            user.setEmail("USER@" + i);
            user.setFirstName("User" + i);
            user.setLastName("User LastName" + i);
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "qwerty" + i));
            userRepository.add(user);
            userList.add(user);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void afterTest() {
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Test
    @DisplayName("Добавление пользователя")
    public void addTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String userFirstName = "User FirstName";
        @NotNull final String userLastName = "User LastName";
        @NotNull final String userLogin = "User Login";
        @NotNull final String userEmail = "User Description";
        @NotNull final UserDTO user = new UserDTO();
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash("123");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final UserDTO createdUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(userFirstName, createdUser.getFirstName());
        Assert.assertEquals(userLastName, createdUser.getLastName());
        Assert.assertEquals(userLogin, createdUser.getLogin());
        Assert.assertEquals(userEmail, createdUser.getEmail());
        userRepository.remove(createdUser);
    }

    @Test
    @DisplayName("Добавление списка пользователей")
    public void addAllTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<UserDTO> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "UserDTO 1 FirstName";
        @NotNull final String firstUserLastName = "UserDTO 1 LastName";
        @NotNull final String firstUserLogin = "UserDTO 1 Login";
        @NotNull final String firstUserEmail = "UserDTO 1 Description";
        @NotNull final UserDTO firstUser = new UserDTO();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        firstUser.setPasswordHash("321");
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "UserDTO 2 FirstName";
        @NotNull final String secondUserLastName = "UserDTO 2 LastName";
        @NotNull final String secondUserLogin = "UserDTO 2 Login";
        @NotNull final String secondUserEmail = "UserDTO 2 Description";
        @NotNull final UserDTO secondUser = new UserDTO();
        secondUser.setFirstName(secondUserFirstName);
        secondUser.setLastName(secondUserLastName);
        secondUser.setLogin(secondUserLogin);
        secondUser.setEmail(secondUserEmail);
        secondUser.setPasswordHash("342");
        users.add(secondUser);
        @NotNull final Collection<UserDTO> addedUserDTOs = userRepository.add(users);
        Assert.assertTrue(addedUserDTOs.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
        userRepository.remove(firstUser);
        userRepository.remove(secondUser);
    }

    @Test
    @DisplayName("Поиск всех пользователей")
    public void findAllTest() {
        @Nullable final List<UserDTO> users = userRepository.findAll();
        Assert.assertNotNull(users);
        Assert.assertTrue(users.size() > 0);
    }

    @Test
    @DisplayName("Поиск пользователя по логину")
    public void findByLoginTest() {
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final UserDTO foundUserDTO = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUserDTO);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null логину")
    public void findByLoginNullTest() {
        @Nullable final UserDTO foundUserDTONull = userRepository.findByLogin(null);
        Assert.assertNull(foundUserDTONull);
        @Nullable final UserDTO foundUserDTO = userRepository.findByLogin("123321");
        Assert.assertNull(foundUserDTO);
    }

    @Test
    @DisplayName("Поиск пользователя по Email")
    public void findByEmailTest() {
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final UserDTO foundUserDTO = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUserDTO);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null Email")
    public void findByEmailNullTest() {
        @Nullable final UserDTO foundUserDTONull = userRepository.findByEmail(null);
        Assert.assertNull(foundUserDTONull);
        @Nullable final UserDTO foundUserDTO = userRepository.findByEmail("123321");
        Assert.assertNull(foundUserDTO);
    }

    @Test
    @DisplayName("Поиск пользователя по Id")
    public void findOneByIdTest() {
        @Nullable UserDTO user;
        for (int i = 0; i < userList.size(); i++) {
            user = userList.get(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final UserDTO foundUserDTO = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUserDTO);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final UserDTO foundUserDTO = userRepository.findOneById("qwerty");
        Assert.assertNull(foundUserDTO);
        @Nullable final UserDTO foundUserDTONull = userRepository.findOneById(null);
        Assert.assertNull(foundUserDTONull);
    }

    @Test
    @DisplayName("Поиск пользователя по индексу")
    public void findOneByIndexTest() {
        for (int i = 1; i <= userList.size(); i++) {
            @Nullable final UserDTO user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    @DisplayName("Поиск пользователя по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final UserDTO user = userRepository.findOneByIndex(null);
        Assert.assertNull(user);
    }

    @Test
    @DisplayName("Получение количества пользователей")
    public void getSizeTest() {
        int actualSize = userRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Удаление пользователя")
    public void removeTest() {
        @Nullable final UserDTO user = userList.get(1);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final UserDTO deletedUserDTO = userRepository.remove(user);
        Assert.assertNotNull(deletedUserDTO);
        @Nullable final UserDTO deletedUserDTOInRepository = userRepository.findOneById(userId);
        Assert.assertNull(deletedUserDTOInRepository);
    }

    @Test
    @DisplayName("Удаление Null пользователя")
    public void removeNullTest() {
        @Nullable final UserDTO user = userRepository.remove(null);
        Assert.assertNull(user);
    }

}