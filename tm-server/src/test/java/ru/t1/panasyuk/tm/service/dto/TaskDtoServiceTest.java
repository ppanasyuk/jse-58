package ru.t1.panasyuk.tm.service.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.*;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.*;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса TaskDtoService")
public class TaskDtoServiceTest extends AbstractSchemeTest {

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private UserDTO test;

    @NotNull
    private UserDTO admin;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = context.getBean(IProjectDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        @NotNull final IProjectTaskDtoService projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        taskList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        @NotNull final ProjectDTO project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final ProjectDTO project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final TaskDTO task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        taskService.update(task1);
        @NotNull final TaskDTO task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        taskService.update(task3);
        taskList.add(task1);
        taskList.add(task3);
    }

    @After
    public void afterTest() throws Exception {
        taskService.clear(test.getId());
        taskService.clear(admin.getId());
        projectService.clear(test.getId());
        projectService.clear(admin.getId());
        userService.remove(admin);
        userService.remove(test);
    }

    @Test
    @DisplayName("Добавление задачи с проверкой пользователя")
    public void AddForUserTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(test.getId());
        task.setName("Test Add");
        task.setDescription("Test Add");
        taskService.add(test.getId(), task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null задачи с проверкой пользователя")
    public void AddNullForUserTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId());
        @Nullable final TaskDTO task = taskService.add(test.getId(), null);
        Assert.assertNull(task);
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Изменение статуса задачи по Id")
    public void changeTaskStatusByIdTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(test.getId());
        Assert.assertNotNull(tasks);
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(test.getId(), taskId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(test.getId(), taskId);
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по пустому Id")
    public void changeTaskStatusByIdTaskIdEmptyTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(test.getId(), "", Status.IN_PROGRESS);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Изменение статуса задачи по Null Id")
    public void changeTaskStatusByIdNullTaskIdEmptyTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Id на некорректный")
    public void changeTaskStatusByIdStatusIncorrectTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(test.getId(), "123", null);
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Изменение статуса несуществующей")
    public void changeTaskStatusByIdTaskNotFoundTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusById(test.getId(), "123", Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Изменение статуса задачи по индексу")
    public void changeTaskStatusByIndexTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(test.getId());
        Assert.assertNotNull(tasks);
        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final TaskDTO task = tasks.get(i);
            @NotNull final String taskId = task.getId();
            @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(test.getId(), i + 1, Status.IN_PROGRESS);
            Assert.assertNotNull(changedTask);
            changedTask = taskService.findOneById(test.getId(), changedTask.getId());
            Assert.assertNotNull(changedTask);
            Assert.assertEquals(Status.IN_PROGRESS, changedTask.getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по Null индексу")
    public void changeTaskStatusByIndexIndexNullTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по отрицательному индексу")
    public void changeTaskStatusByIndexIndexMinusTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(test.getId(), -1, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу на Null")
    public void changeTaskStatusByIIndexStatusIncorrectTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(test.getId(), 1, null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса задачи по индексу больше количества задач")
    public void changeTaskStatusByIndexIndexIncorrectTestNegative() throws Exception {
        @Nullable TaskDTO changedTask = taskService.changeTaskStatusByIndex(test.getId(), 100, Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Удаление всех задач пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(taskService.getSize(test.getId()) > 0);
        taskService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создание задачи с именем и описанием")
    public void createTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @NotNull final String description = "Task Description";
        @Nullable TaskDTO createdTask = taskService.create(test.getId(), name, description);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(description, createdTask.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
        taskService.removeById(test.getId(), createdTask.getId());
    }

    @Test
    @DisplayName("Создание задачи с именем")
    public void createByNameTest() throws Exception {
        int expectedNumberOfEntries = taskService.getSize(test.getId()) + 1;
        @NotNull final String name = "Task name";
        @Nullable TaskDTO createdTask = taskService.create(test.getId(), name);
        @NotNull final String taskId = createdTask.getId();
        Assert.assertNotNull(createdTask);
        createdTask = taskService.findOneById(test.getId(), taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(expectedNumberOfEntries, taskService.getSize(test.getId()));
        taskService.removeById(test.getId(), createdTask.getId());
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем и описанием")
    public void createNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем и описанием")
    public void createNullNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), null, "description");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с пустым описанием")
    public void createDescriptionEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "name", "");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создание задачи с Null описанием")
    public void createNullDescriptionEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "name", null);
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с пустым именем")
    public void createByNameNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), "");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создание задачи с Null именем")
    public void createByNameNullNameEmptyTestNegative() throws Exception {
        taskService.create(test.getId(), null);
    }

    @Test
    @DisplayName("Проверка существования задачи по Id для пользователя")
    public void existByIdTrueForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            final boolean isExist = taskService.existsById(test.getId(), task.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверка несуществования задачи по Id для пользователя")
    public void existByIdFalseUserTest() throws Exception {
        final boolean isExist = taskService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все задачи по Id проекта")
    public void findAllByProjectIdTest() throws Exception {
        @Nullable final ProjectDTO project = projectService.findOneByIndex(test.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<TaskDTO> tasksToFind = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(test.getId(), projectId);
        Assert.assertEquals(tasksToFind, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по пустому Id проекта")
    public void findAllByProjectIdEmptyTest() throws Exception {
        @NotNull final List<TaskDTO> emptyList = Collections.emptyList();
        @NotNull final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(test.getId(), "");
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи по Null Id проекта")
    public void findAllByProjectIdNullTest() throws Exception {
        @NotNull final List<TaskDTO> emptyList = Collections.emptyList();
        @NotNull final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(test.getId(), null);
        Assert.assertEquals(emptyList, tasksByProjectId);
    }

    @Test
    @DisplayName("Найти все задачи")
    public void findAllTest() throws Exception {
        @NotNull final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @DisplayName("Найти все задачи для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<TaskDTO> tasks = taskService.findAll(test.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с компаратором для пользователей")
    public void findAllWithComparatorForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<TaskDTO> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<TaskDTO> tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskService.findAll(test.getId(), comparator);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти все задачи с сортировкой для пользователей")
    public void findAllWithSortForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<TaskDTO> tasks = taskService.findAll(test.getId(), Sort.BY_NAME);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(test.getId(), Sort.BY_CREATED);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        tasks = taskService.findAll(test.getId(), Sort.BY_STATUS);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
        @Nullable final Sort sort = null;
        tasks = taskService.findAll(test.getId(), sort);
        Assert.assertEquals(tasksForTestUser.size(), tasks.size());
    }

    @Test
    @DisplayName("Найти задачу по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable TaskDTO foundTask;
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            foundTask = taskService.findOneById(test.getId(), task.getId());
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    @DisplayName("Найти задачу по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final TaskDTO foundTask = taskService.findOneById(test.getId(), null);
        Assert.assertNull(foundTask);
    }

    @Test
    @DisplayName("Найти задачу по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= tasksForTestUser.size(); i++) {
            @Nullable final TaskDTO task = taskService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(task);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по индексу превышающему количество задач для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() throws Exception {
        int index = taskService.getSize(test.getId()) + 1;
        @Nullable final TaskDTO task = taskService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() throws Exception {
        @Nullable final TaskDTO task = taskService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти задачу по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() throws Exception {
        @Nullable final TaskDTO task = taskService.findOneByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Получить количество задач для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = taskService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить задачи по Id проекта")
    public void removeAllByProjectIdTest() throws Exception {
        @Nullable final ProjectDTO project = projectService.findOneByIndex(test.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<TaskDTO> tasksByProjectId = taskService.findAllByProjectId(test.getId(), projectId);
        Assert.assertTrue(tasksByProjectId.size() > 0);
        taskService.removeAllByProjectId(test.getId(), projectId);
        @Nullable final List<TaskDTO> tasksAfterDelete = taskService.findAllByProjectId(test.getId(), projectId);
        Assert.assertNotNull(tasksAfterDelete);
        Assert.assertEquals(0, tasksAfterDelete.size());
    }

    @Test
    @DisplayName("Удалить задачу по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        @NotNull final List<TaskDTO> tasksForTestUser = taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final TaskDTO task : tasksForTestUser) {
            @NotNull final String taskId = task.getId();
            @Nullable final TaskDTO deletedTask = taskService.removeById(test.getId(), taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final TaskDTO deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() throws Exception {
        taskService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удалить задачу по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() throws Exception {
        taskService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удалить несуществующую задачу по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() throws Exception {
        taskService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить задачу по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) taskList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final TaskDTO deletedTask = taskService.removeByIndex(test.getId(), index);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final TaskDTO deletedTaskInRepository = taskService.findOneById(test.getId(), taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по индексу больше количества задач для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() throws Exception {
        int index = taskList.size() + 1;
        taskService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить задачу по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() throws Exception {
        taskService.removeByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Обновить задачу по Id")
    public void updateByIdTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(test.getId());
        Assert.assertNotNull(tasks);
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final TaskDTO task : tasks) {
            @NotNull final String taskId = task.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable TaskDTO updatedTask = taskService.updateById(test.getId(), taskId, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(test.getId(), taskId);
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test
    @DisplayName("Обновить задачу по индексу")
    public void updateByIndexTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(test.getId());
        Assert.assertNotNull(tasks);
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 1;
        for (@NotNull final TaskDTO task : tasks) {
            name = "name " + index;
            description = "description" + index;
            @Nullable TaskDTO updatedTask = taskService.updateByIndex(test.getId(), index, name, description);
            Assert.assertNotNull(updatedTask);
            updatedTask = taskService.findOneById(test.getId(), updatedTask.getId());
            Assert.assertNotNull(updatedTask);
            Assert.assertEquals(name, updatedTask.getName());
            Assert.assertEquals(description, updatedTask.getDescription());
            index++;
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по пустому Id")
    public void UpdateByIdIdEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "", "name", "description");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновить задачу по Null Id")
    public void UpdateByIdNullIdEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), null, "name", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и пустым именем")
    public void UpdateByIdNameEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "id", "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по Id и Null именем")
    public void UpdateByIdNullNameEmptyTestNegative() throws Exception {
        taskService.updateById(test.getId(), "id", null, "description");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Обновить несуществующую задачу по Id")
    public void UpdateByIdTaskNotFoundTestNegative() throws Exception {
        taskService.updateById(test.getId(), "123", "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по Null индексу")
    public void UpdateByIndexIndexNullTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), null, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по отрицательному индексу")
    public void UpdateByIndexMinusTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), -1, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновить задачу по индексу больше количества задач")
    public void UpdateByIndexIndexIncorrectTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 100, "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с Null именем")
    public void UpdateByIndexNullNameEmptyTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 1, null, "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновить задачу по индексу с пустым именем")
    public void UpdateByIndexNameEmptyTestNegative() throws Exception {
        taskService.updateByIndex(test.getId(), 1, "", "description");
    }

}