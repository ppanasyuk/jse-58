package ru.t1.panasyuk.tm.repository.dto;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.configuration.ServerConfiguration;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория проектов DTO")
public class ProjectDtoRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private String testUser1Id;

    @NotNull
    private String testUser2Id;

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    private IProjectDtoRepository projectRepository;

    @NotNull
    private IUserDtoRepository userRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws LiquibaseException, SQLException, IOException {
        liquibase.update("scheme");
        context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        userRepository = context.getBean(IUserDtoRepository.class);
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        projectRepository = context.getBean(IProjectDtoRepository.class);
        @NotNull final UserDTO user1 = new UserDTO();
        testUser1Id = user1.getId();
        @NotNull final UserDTO user2 = new UserDTO();
        testUser2Id = user2.getId();
        userRepository.add(user1);
        userRepository.add(user2);
        entityManager.getTransaction().commit();
        entityManager = projectRepository.getEntityManager();
        entityManager.getTransaction().begin();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(testUser1Id);
            else project.setUserId(testUser2Id);
            projectList.add(project);
            projectRepository.add(project);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void afterTest() {
        projectRepository.clear();
        entityManager.getTransaction().commit();
        entityManager = userRepository.getEntityManager();
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

//    @Test
//    public void findTest() {
//        @Nullable final List<ProjectDTO> projects = projectRepository.findAll("a9a012d9-bfa4-401c-9ba7-2f75e278b6eb");
//    }

    @Test
    @DisplayName("Добавление проекта")
    public void addTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "Project Name";
        @NotNull final String projectDescription = "Project Description";
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName(projectName);
        project.setDescription(projectDescription);
        @Nullable final ProjectDTO createdProject = projectRepository.add(testUser1Id, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(testUser1Id, createdProject.getUserId());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    @DisplayName("Добавление Null проекта")
    public void addNullTest() {
        @Nullable final ProjectDTO createdProjectDTO = projectRepository.add(testUser1Id, null);
        Assert.assertNull(createdProjectDTO);
    }

    @Test
    @DisplayName("Добавление списока проектов")
    public void addAllTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<ProjectDTO> projects = new ArrayList<>();
        @NotNull final String firstProjectDTOName = "First ProjectDTO Name";
        @NotNull final String firstProjectDTODescription = "ProjectDTO Description";
        @NotNull final ProjectDTO firstProjectDTO = new ProjectDTO();
        firstProjectDTO.setName(firstProjectDTOName);
        firstProjectDTO.setDescription(firstProjectDTODescription);
        firstProjectDTO.setUserId(testUser1Id);
        projects.add(firstProjectDTO);
        @NotNull final String secondProjectDTOName = "Second ProjectDTO Name";
        @NotNull final String secondProjectDTODescription = "ProjectDTO Description";
        @NotNull final ProjectDTO secondProjectDTO = new ProjectDTO();
        secondProjectDTO.setName(secondProjectDTOName);
        secondProjectDTO.setDescription(secondProjectDTODescription);
        secondProjectDTO.setUserId(testUser1Id);
        projects.add(secondProjectDTO);
        @NotNull final Collection<ProjectDTO> addedProjectDTOs = projectRepository.add(projects);
        Assert.assertTrue(addedProjectDTOs.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удаление проектов для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(testUser1Id);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(testUser1Id));
    }

    @Test
    @DisplayName("Поиск всех проектов")
    public void findAllTest() {
        @NotNull int allProjectDTOsSize = projectRepository.findAll().size();
        @Nullable final List<ProjectDTO> projects = projectRepository.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору")
    public void findAllWithComparatorTest() {
        @NotNull Comparator<ProjectDTO> comparator = Sort.BY_NAME.getComparator();
        int allProjectDTOsSize = projectRepository.findAll().size();
        @Nullable List<ProjectDTO> projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(allProjectDTOsSize, projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов для пользователя")
    public void findAllForUserTest() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable final List<ProjectDTO> projects = projectRepository.findAll(testUser1Id);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    @DisplayName("Поиск всех проектов по компаратору для пользователя")
    public void findAllWithComparatorForUser() {
        @NotNull final List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<ProjectDTO> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<ProjectDTO> projects = projectRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(testUser1Id, comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectListForUser.size(), projects.size());
    }

    @Test
    @DisplayName("Поиск проекта по Id")
    public void findOneByIdTest() {
        @Nullable ProjectDTO project;
        for (int i = 1; i <= projectList.size(); i++) {
            project = projectList.get(i - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProjectDTO);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById("qwerty");
        Assert.assertNull(foundProjectDTO);
        @Nullable final ProjectDTO foundProjectDTONull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectDTONull);
    }

    @Test
    @DisplayName("Поиск проекта по Id для пользователя")
    public void findOneByIdForUserTest() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final ProjectDTO project : projectListForUser) {
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(testUser1Id, projectId);
            Assert.assertNotNull(foundProjectDTO);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final ProjectDTO foundProjectDTO = projectRepository.findOneById(testUser1Id, "qwerty");
        Assert.assertNull(foundProjectDTO);
        @Nullable final ProjectDTO foundProjectDTONull = projectRepository.findOneById(testUser1Id, null);
        Assert.assertNull(foundProjectDTONull);
    }

    @Test
    @DisplayName("Поиск проекта по индексу")
    public void findOneByIndexTest() {
        for (int i = 1; i <= projectList.size(); i++) {
            @Nullable final ProjectDTO project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final ProjectDTO project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Поиск проекта по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull List<ProjectDTO> projectListForUser = projectList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectListForUser.size(); i++) {
            @Nullable final ProjectDTO project = projectRepository.findOneByIndex(testUser1Id, i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    @DisplayName("Поиск проекта по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() {
        @Nullable final ProjectDTO project = projectRepository.findOneByIndex(testUser1Id, null);
        Assert.assertNull(project);
    }

    @Test
    @DisplayName("Получить количество проектов")
    public void getSizeTest() {
        int actualSize = projectRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получить количество проектов для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> testUser1Id.equals(m.getUserId()))
                .count();
        int actualSize = projectRepository.getSize(testUser1Id);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удалить проект")
    public void removeTest() throws Exception {
        @Nullable final ProjectDTO project = projectList.get(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final ProjectDTO deletedProjectDTO = projectRepository.remove(project);
        Assert.assertNotNull(deletedProjectDTO);
        @Nullable final ProjectDTO deletedProjectDTOInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectDTOInRepository);
    }

    @Test
    @DisplayName("Удалить Null проект")
    public void removeNullTest() {
        @Nullable final ProjectDTO project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

}