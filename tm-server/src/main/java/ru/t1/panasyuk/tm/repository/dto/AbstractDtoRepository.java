package ru.t1.panasyuk.tm.repository.dto;

import lombok.Getter;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.repository.dto.IDtoRepository;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.*;

@Repository
@Scope("prototype")
public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @Getter
    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getEntityClass();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return FieldConst.FIELD_NAME;
        if (comparator == StatusComparator.INSTANCE) return FieldConst.FIELD_STATUS;
        return FieldConst.FIELD_CREATED;
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        for (@NotNull final M model : models)
            add(model);
        return models;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m",
                getEntityClass().getSimpleName()
        );
        entityManager.createQuery(jpql).executeUpdate();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final String jpql = String.format(
                "FROM %s m ORDER BY m.%s DESC",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        @NotNull final String jpql = String.format(
                "FROM %s m ORDER BY m.%s ASC",
                getEntityClass().getSimpleName(),
                getSortType(comparator)
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null) return null;
        return entityManager.find(getEntityClass(), id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null) return null;
        @NotNull final String jpql = String.format(
                "FROM %s m ORDER BY m.%s",
                getEntityClass().getSimpleName(),
                FieldConst.FIELD_CREATED
        );
        return entityManager
                .createQuery(jpql, getEntityClass())
                .setFirstResult(index - 1)
                .setMaxResults(1)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = String.format(
                "SELECT COUNT(m) FROM %s m",
                getEntityClass().getSimpleName()
        );
        return entityManager
                .createQuery(jpql, Long.class)
                .getSingleResult()
                .intValue();
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        entityManager.remove(model);
        return model;
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}