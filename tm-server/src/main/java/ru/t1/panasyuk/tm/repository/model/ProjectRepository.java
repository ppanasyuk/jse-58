package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.api.repository.model.IProjectRepository;
import ru.t1.panasyuk.tm.model.Project;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    protected @NotNull Class<Project> getEntityClass() {
        return Project.class;
    }

}